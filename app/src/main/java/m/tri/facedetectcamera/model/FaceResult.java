package m.tri.facedetectcamera.model;

import android.graphics.PointF;

public class FaceResult extends Object {

    private PointF midEye;
    private float eyeDist;
    private float confidence;
    private float pose;
    private int id;
    private long time;
    private float euler;

    public FaceResult() {
        id = 0;
        midEye = new PointF(0.0f, 0.0f);
        eyeDist = 0.0f;
        confidence = 0.4f;
        pose = 0.0f;
        time = System.currentTimeMillis();
        euler = 0;
    }


    public void setFace(int id, PointF midEye, float eyeDist, float confidence, float pose, long time, float euler) {
        set(id, midEye, eyeDist, confidence, pose, time, euler);
    }

    public void clear() {
        set(0, new PointF(0.0f, 0.0f), 0.0f, 0.4f, 0.0f, System.currentTimeMillis(), 0);
    }

    public synchronized void set(int id, PointF midEye, float eyeDist, float confidence, float pose, long time, float euler) {
        this.id = id;
        this.midEye.set(midEye);
        this.eyeDist = eyeDist;
        this.confidence = confidence;
        this.pose = pose;
        this.time = time;
        this.euler = euler;
    }

    public float eyesDistance() {
        return eyeDist;
    }

    public void setEyeDist(float eyeDist) {
        this.eyeDist = eyeDist;
    }

    public void getMidPoint(PointF pt) {
        pt.set(midEye);
    }

    public PointF getMidEye() {
        return midEye;
    }

    public void setMidEye(PointF midEye) {
        this.midEye = midEye;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    public float getPose() {
        return pose;
    }
    public float getEuler(){return euler;}
    public void setEuler(float euler) {this.euler = euler;}

    public void setPose(float pose) {
        this.pose = pose;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
